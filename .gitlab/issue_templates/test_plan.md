# Failover Test Plan

## README

If you are volunteering, please make yourself available to join the **GCP Migration Rehearsal** call. Ping @meks for an invite.

* Zoom link: https://gitlab.zoom.us/j/859814316
* Slack channel: [#gcp_migration](https://gitlab.slack.com/messages/C7S4KUEPN)

## High-level Overview

This test plan requires some manual testing as well as some automated testing using [GitLab QA].

The automated and manual QA processes can be conducted in parallel with one another.

The plan will also involve testing during two distinct phases of the failover plan:

- **During the Blackout** (inside the maintenance window period):
  - All the tests conducted during this period are related to features critical to the operation of GitLab.com.
  - **The maintenance window cannot be closed until these tests pass.**
- **After the Blackout** (after the maintenance window period):
  - These tests are non-critical, but, in order to keep the maintenance window as short as possible, should be conducted after the new failover GitLab instance is public.

## Automated QA

### During the Blackout

@meks / @remy to run [GitLab QA] against failed-over environment.

- [ ] Make sure to export the following environment variables

  ```
  export GITLAB_USERNAME=gitlab-qa GITLAB_PASSWORD=xxx
  ```
- [ ] Automated QA completed

  Until https://gitlab.com/gitlab-org/gitlab-qa/issues/272 is resolved, you'll need to use the [following workaround](https://gitlab.com/gitlab-org/gitlab-qa/issues/272#workaround):

  ```
  # Make sure to replace `11.0.0-rc8-ee` with the version exposed at http://staging.gitlab.com/help
  # In gitlab-ee
  › git checkout v11.0.0-rc8-ee
  › cd qa
  › docker build -t gitlab/gitlab-ee-qa:11.0.0-rc8-ee .
  › gitlab-qa Test::Instance::Any gitlab/gitlab-ee 11.0.0-rc8-ee https://staging.gitlab.com
  ```

- [ ] Post results and failures logs + screenshots as comments of this issue
- [ ] Create follow-up issues for all the automated QA failures

## Manual QA

------------

PLEASE FOLLOW THESE INSTRUCTIONS AND REMOVE THEM ONCE DONE.

@meks / @remy to follow the following steps:

1. Open https://docs.google.com/spreadsheets/d/15AtBb6s2p_HvtUe5G9GUSc2ngt69X8dO-418zMuT4us/edit#gid=0
1. Duplicate the document and call it `GCP Migration Manual Testing - yyyy-mm-dd`
1. Set the permissions so that **everyone at GitLab can edit**.
1. Replace all the occurrences of `LINK_TO_MANUAL_TESTPLAN` below with a link to the test plan here.

------------

**Manual test plan:** LINK_TO_MANUAL_TESTPLAN

### During the Blackout

- [ ] When the 🔪 Chef-Runner is at the `Ensure that unicorn, etc, has been
  restarted on all hosts` step, send a heads-to QA testers in `#gcp_migration`:

  ```
  @mkozono @bob @fran @bwalker @felipe_artur @dgriffith @DaveSmith @Daniel Gruesso @toon @ruben @jedwardsjones @fabio Heads-up, manual QA testing will start soon. Please be ready (test plan: LINK_TO_MANUAL_TESTPLAN)! :)
  ```

- [ ] Post in `#gcp_migration`:

  ```
  @mkozono @bob @fran @bwalker @felipe_artur @dgriffith @DaveSmith You can start performing your respective "During Blackout" manual QA scenarios on https://staging.gitlab.com. Please find the scenarios and track results at LINK_TO_MANUAL_TESTPLAN. Thanks in advance! :tada:
  ```

- [ ] Create follow-up issues for all the manual QA failures

### After the Blackout

- [ ] Post in `#gcp_migration`:

  ```
  @felipe_artur @dgriffith @Daniel Gruesso @toon @bwalker @DaveSmith @ruben @jedwardsjones @fabio You can start performing your respective "After Blackout" manual QA scenarios on https://staging.gitlab.com. Please find the scenarios and track results at LINK_TO_MANUAL_TESTPLAN. Thanks in advance! :tada:
  ```

- [ ] Create follow-up issues for all the manual QA failures

- [ ] Post the deadline to perform "After Blackout" manual QA in `#gcp_migration` (make sure to replace `X:Y UTC` by the actual time):

  ```
  @felipe_artur @dgriffith @Daniel Gruesso @toon @bwalker @DaveSmith @ruben @jedwardsjones @fabio FYI the deadline for performing the "After Blackout" manual QA is set to X:Y UTC. Thanks in advance! :tada:
  ```

[GitLab QA]: https://gitlab.com/gitlab-org/gitlab-qa

/label ~"Failover Execution"
