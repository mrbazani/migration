# How to manually verify attachments

## Compare checksum in DB vs recalculated using the file

This will also identify uploads with missing files, and orphaned uploads, by looking the exceptions.

```
sudo gitlab-rake gitlab:uploads:check BATCH=1000 VERBOSE=1
```

Example output:

```
mkozono@sidekiq-besteffort-03-sv-gprd.c.gitlab-production.internal:~$ sudo gitlab-rake gitlab:uploads:check BATCH=1000 VERBOSE=1
Checking integrity of Uploads
- 1..1076: Failures: 0
- 1077..2153: Failures: 0
- 2154..3270: Failures: 0
- 3271..4423: Failures: 0
- 4424..5532: Failures: 1
  - Upload: 4636: #<Errno::ENOENT: No such file or directory @ rb_sysopen - /opt/gitlab/embedded/service/gitlab-rails/public/uploads/some_group/some_project/a252e6d7cb6ec1e97bb726be435f4c18/Image.png>
- 5533..6614: Failures: 4
  - Upload: 5812: #<NoMethodError: undefined method `hashed_storage?' for nil:NilClass>
  - Upload: 5837: #<NoMethodError: undefined method `hashed_storage?' for nil:NilClass>
  - Upload: 5850: #<NoMethodError: undefined method `hashed_storage?' for nil:NilClass>
  - Upload: 5854: #<NoMethodError: undefined method `hashed_storage?' for nil:NilClass>
- 6615..7693: Failures: 0
- 7694..8853: Failures: 0
```

## Get IDs of uploads missing their files, orphaned uploads, missing checksums, and mismatched checksums

Save the output of `gitlab:uploads:check VERBOSE=1` into `verify_uploads_output`, and then run:

```
grep "No such file" verify_uploads_output | awk '{print $3}' | sed 's/://' > upload_ids_missing_file
grep "hashed_storage?" verify_uploads_output | awk '{print $3}' | sed 's/://' > upload_ids_orphaned
grep "Checksum missing" verify_uploads_output | awk '{print $3}' | sed 's/://' > upload_ids_missing_checksum
grep "Checksum mismatch" verify_uploads_output | awk '{print $3}' | sed 's/://' > upload_ids_mismatched_checksum
```

## On a secondary, get registries of uploads missing files, that are synced and not marked as missing on primary

First, perform the above, and get the IDs of uploads missing files into an array
in Rails console on the secondary. If there are not too many IDs, here is an
easy way-- Just paste the line-separated numbers into Rails console:

```ruby
upload_ids = %w[
  1583
  1589
  ...
  2405961
  2405978
]
```

Then get matching registries, and scope as desired:

```ruby
registries = Geo::FileRegistry.attachments.synced.where(file_id: upload_ids, missing_on_primary: false); nil
```
